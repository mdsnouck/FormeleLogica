package foyolo;
import java.util.ArrayList;
/**
 * @author Maarten Desnouck
 * 
 * WIT = 1
 * ZWART = 0
 * 
 */
public class FOYOLO {
    public static void main(String[] args) {
        String oplossing = "";
        
        String NOT = "~";
        String AND = " & ";
        String OR = " | ";
        /*String NOT = " \\not ";
        String AND = " \\land ";
        String OR = " \\lor ";*/
        
        int grootte = 3;
        int aantalVakjes = (int)Math.pow(grootte,2);
        int aantalConfigs = (int)Math.pow(2,grootte*grootte);
        int pijlen[] = {4,5,5,3,5,7,2,1,7};
        int waardes[] = {1,1,1,1,1,2,2,2,0};
        
        for(int bit = 0; bit<aantalVakjes; bit++){
            int x = bit%grootte;
            int y = bit/grootte;
            int huidigePijl = pijlen[bit];
            int huidigeWaarde = waardes[bit];
            ArrayList<Vakje> bereik = bereik(grootte, x, y, huidigePijl);

            System.out.print("((");
            String kleur = "zwart";
            //Als het vakje zwart is
            //Aantal witte vakjes in het bereik != huidigeWaarde

            ArrayList<String> powerset = powerSet(huidigeWaarde, bereik, kleur);
            int aantalSets = powerset.size();

            for(int set = 0; set<aantalSets; set++){
                String dezeSet = powerset.get(set);
                System.out.print("(");
                
                //Het vakje zelf
                //System.out.print(NOT+"x_"+x+"{}_"+y+AND);
                System.out.print(NOT+"x"+x+y+AND);
                
                //De regel
                for(int vakje=0;vakje<bereik.size();vakje++){
                    if(dezeSet.charAt(vakje)=='1'){
                        //System.out.print("x_"+bereik.get(vakje).x+"{}_"+bereik.get(vakje).y);
                        System.out.print("x"+bereik.get(vakje).x+bereik.get(vakje).y);
                    } else {
                        //System.out.print(NOT+"x_"+bereik.get(vakje).x+"{}_"+bereik.get(vakje).y);
                        System.out.print(NOT+"x"+bereik.get(vakje).x+bereik.get(vakje).y);
                    }
                    if(vakje<bereik.size()-1){
                        System.out.print(AND);
                    }
                }
                System.out.print(")");
                if(set<aantalSets-1){
                    System.out.print(OR);
                }
            }
            
            System.out.print(")"+OR+"(");

            kleur = "wit";
            //Als het vakje wit is 
            //Aantal witte vakjes in bereik == huidigeWaarde

            powerset = powerSet(huidigeWaarde, bereik, kleur);
            aantalSets = powerset.size();

            for(int set = 0; set<aantalSets; set++){
                String dezeSet = powerset.get(set);
                System.out.print("(");
                
                //Het vakje zelf
                //System.out.print("x_"+x+"{}_"+y+AND);
                System.out.print("x"+x+y+AND);
                
                //De regel
                for(int vakje=0;vakje<bereik.size();vakje++){
                    if(dezeSet.charAt(vakje)=='1'){
                        //System.out.print("x_"+bereik.get(vakje).x+"{}_"+bereik.get(vakje).y);
                        System.out.print("x"+bereik.get(vakje).x+bereik.get(vakje).y);
                    } else {
                        //System.out.print(NOT+"x_"+bereik.get(vakje).x+"{}_"+bereik.get(vakje).y);
                        System.out.print(NOT+"x"+bereik.get(vakje).x+bereik.get(vakje).y);
                    }
                    if(vakje<bereik.size()-1){
                        System.out.print(AND);
                    }

                }
                System.out.print(")");
                if(set<aantalSets-1){
                    System.out.print(OR);
                }
            }
            System.out.print("))");
            if(bit<aantalVakjes-1){
               System.out.print(AND);
            }
   
        }
    }
    
    public static ArrayList<Vakje> bereik(int grootte, int x, int y, int pijl){
    ArrayList<Vakje> bereik = new ArrayList<>();
    int huidigeX = x;
    int huidigeY = y;

    switch (pijl) {
        case 1:  //NOORD
            while(huidigeY>=1){
                huidigeY--;
                String waarde = 'X'+Integer.toString(huidigeX)+Integer.toString(huidigeY);
                Vakje coord = new Vakje();
                coord.x = huidigeX;
                coord.y = huidigeY;
                bereik.add(coord);    
            }
            break;
        case 2: //NO 
            while(huidigeY>=1 && huidigeX<grootte-1){
                huidigeY--;
                huidigeX++;
                String waarde = 'X'+Integer.toString(huidigeX)+Integer.toString(huidigeY);
                Vakje coord = new Vakje();
                coord.x = huidigeX;
                coord.y = huidigeY;
                bereik.add(coord); 
            }
            break;
        case 3: //OOST
            while(huidigeX<grootte-1){
                huidigeX++;
                String waarde = 'X'+Integer.toString(huidigeX)+Integer.toString(huidigeY);
                Vakje coord = new Vakje();
                coord.x = huidigeX;
                coord.y = huidigeY;
                bereik.add(coord);  
            }
            break;
        case 4: //ZO
            while(huidigeY<grootte-1 && huidigeX<grootte-1){
                huidigeY++;
                huidigeX++;
                String waarde = 'X'+Integer.toString(huidigeX)+Integer.toString(huidigeY);
                Vakje coord = new Vakje();
                coord.x = huidigeX;
                coord.y = huidigeY;
                bereik.add(coord);  
            }
            break;
        case 5:  //ZUID
            while(huidigeY<grootte-1){
                huidigeY++;
                String waarde = 'X'+Integer.toString(huidigeX)+Integer.toString(huidigeY);
                Vakje coord = new Vakje();
                coord.x = huidigeX;
                coord.y = huidigeY;
                bereik.add(coord);  
            }
            break;
        case 6: //ZW 
            while(huidigeY<grootte-1 && huidigeX>=1){
                huidigeY++;
                huidigeX--;
                String waarde = 'X'+Integer.toString(huidigeX)+Integer.toString(huidigeY);
                Vakje coord = new Vakje();
                coord.x = huidigeX;
                coord.y = huidigeY;
                bereik.add(coord);  
            }
            break;
        case 7: //WEST
            while(huidigeX>=1){
                huidigeX--;
                String waarde = 'X'+Integer.toString(huidigeX)+Integer.toString(huidigeY);
                Vakje coord = new Vakje();
                coord.x = huidigeX;
                coord.y = huidigeY;
                bereik.add(coord);  
            }
            break;
        case 8: //NW
            while(huidigeY>=1 && huidigeX>=1){
                huidigeY--;
                huidigeX--;
                String waarde = 'X'+Integer.toString(huidigeX)+Integer.toString(huidigeY);
                Vakje coord = new Vakje();
                coord.x = huidigeX;
                coord.y = huidigeY;
                bereik.add(coord);  
            }
            break;
     }   
    return bereik;
}
    
    public static ArrayList<String> powerSet(int huidigeWaarde, ArrayList<Vakje> verzameling, String kleur){
    ArrayList<String> result = new ArrayList<>();
    int grootte = verzameling.size();
    
    for(int config = 0; config< Math.pow(2,grootte); config++){
        String configuratie = String.format("%"+grootte+"s", Integer.toBinaryString(config)).replace(' ', '0');
        int aantalBits = 0;
        
        
        //tellen hoeveel bits in dit getal maw hoeveel elementen in deze subset
        for(int bit = 0; bit<grootte;  bit++){
            if(configuratie.charAt(bit)=='1'){
                aantalBits++;
            }
        }
        
        //een subset maken als het aantalbits in de bitvector juist is
        if(kleur.equals("wit")){
        //wit : als aantal klopt dan mag het in de verzameling
            if(aantalBits == huidigeWaarde){
                result.add(configuratie);
            }
            
        } else {
        //zwart : als aantal NIET klopt dan mag het in de verzameling
            if(aantalBits != huidigeWaarde){
                result.add(configuratie);
            }
        }
    }       
    return result;
}

}

class Vakje {
    public int x;
    public int y;
    public int kleur;
}
